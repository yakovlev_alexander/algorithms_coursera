
Programming assignments for Algorithms Part I:  

-  1 week - Union-find algorithms (Percolation.java and PercolationStats.java) - 94/100 points.
-  2 week - Stack and Queue Applications (Deque.java, RandomizedQueue.java, and Permutation.java) - 91/100 points.
-  3 week - Collinear Points (Point.java, BruteCollinearPoints.java, and FastCollinearPoints.java) - 81/100 points.



Supplementary library: https://github.com/kevin-wayne/algs4