package org.coursera.lesson02;

import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private int size;
    private Item[] arr;

    // construct an empty randomized queue
    public RandomizedQueue() {
        arr = (Item[]) new Object[10];
    }

    private RandomizedQueue(int size, Item[] arr) {
        this.size = size;
        this.arr = (Item[]) new Object[size + 1];
        for (int i = 0; i <= size; i++) {
            this.arr[i] = arr[i];
        }
    }

    // is the queue empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // return the number of items on the queue
    public int size() {
        return size;
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
        arr[size++] = item;
        if (size == arr.length - 1) {
            resize(arr.length * 2);
        }
    }

    // remove and return a random item
    public Item dequeue() {
        checkSize();
        int returnIndex = StdRandom.uniform(0, size);
        Item returnItem = arr[returnIndex];
        for (int i = returnIndex; i < size; i++) {
            arr[i] = arr[i + 1];
        }
        size--;
        if (size > 0 && size == arr.length / 4) {
            resize(arr.length / 2);
        }
        return returnItem;
    }

    private void checkSize() {
        if (size == 0) {
            throw new NoSuchElementException();
        }
    }

    // return (but do not remove) a random item
    public Item sample() {
        checkSize();
        return arr[StdRandom.uniform(0, size)];

    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new IteratorImpl<Item>(this);

    }

    private void resize(int newSize) {
        Item[] newArr = (Item[]) new Object[newSize];
        int i = 0;
        for (Item item : arr) {
            if (item == null) {
                break;
            }
            newArr[i++] = item;
        }
        arr = newArr;
    }

    private static class IteratorImpl<Item> implements Iterator<Item> {

        private RandomizedQueue<Item> queue;

        public IteratorImpl(RandomizedQueue<Item> queue) {
            this.queue = new RandomizedQueue<Item>(queue.size, queue.arr);
        }

        public boolean hasNext() {
            return !queue.isEmpty();
        }

        public Item next() {
            if (queue.isEmpty()) {
                throw new NoSuchElementException();
            }
            return queue.dequeue();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public static void main(String[] args) {
        RandomizedQueue<Integer> integers = new RandomizedQueue<Integer>();

        for (int i = 0; i < 100; i++) {
            integers.enqueue(i);
        }

        for (Integer inter : integers) {
            System.out.println(inter);
        }
    }
}
