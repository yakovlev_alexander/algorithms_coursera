package org.coursera.lesson02;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private int size;
    private Node<Item> firstNode;
    private Node<Item> lastNode;

    // construct an empty deque
    public Deque() {
    }

    // is the deque empty?
    public boolean isEmpty() {
        return size == 0;
    }

    // return the number of items on the deque
    public int size() {
        return size;
    }

    // add the item to the front
    public void addFirst(Item item) {
        checkNull(item);
        Node<Item> newNode = new Node<Item>(item);
        if (firstNode == null) {
            firstNode = newNode;
            lastNode = newNode;
        } else {
            newNode.nextNode = firstNode;
            firstNode.prevNode = newNode;
            firstNode = newNode;
        }
        size++;
    }

    // add the item to the end
    public void addLast(Item item) {
        checkNull(item);
        Node<Item> newNode = new Node<Item>(item);
        if (lastNode == null) {
            firstNode = newNode;
            lastNode = newNode;
        } else {
            newNode.prevNode = lastNode;
            lastNode.nextNode = newNode;
            lastNode = newNode;
        }
        size++;
    }

    // remove and return the item from the front
    public Item removeFirst() {
        checkSize(size);
        Item returnItem = firstNode.item;
        if (firstNode == lastNode) {
            firstNode = null;
            lastNode = null;
        } else {
            firstNode = firstNode.nextNode;
            firstNode.prevNode = null;
        }
        size--;
        return returnItem;
    }

    // remove and return the item from the end
    public Item removeLast() {
        checkSize(size);
        Item returnItem = lastNode.item;
        if (firstNode == lastNode) {
            firstNode = null;
            lastNode = null;
        } else {
            lastNode = lastNode.prevNode;
            lastNode.nextNode = null;
        }
        size--;
        return returnItem;
    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new IteratorImpl<Item>(firstNode);
    }

    private static <Item> void checkNull(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }
    }

    private static void checkSize(int size) {
        if (size == 0) {
            throw new NoSuchElementException();
        }
    }

    private static class Node<Item> {
        private Item item;
        private Node<Item> nextNode;
        private Node<Item> prevNode;

        public Node(Item item) {
            this.item = item;
        }
    }

    private static class IteratorImpl<Item> implements Iterator<Item> {

        private Node<Item> currNode;

        public IteratorImpl(Node<Item> currNode) {
            this.currNode = currNode;
        }

        public boolean hasNext() {
            return currNode != null;
        }

        public Item next() {
            if (currNode == null) {
                throw new NoSuchElementException();
            }
            Item returnItem = currNode.item;
            currNode = currNode.nextNode;
            return returnItem;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
