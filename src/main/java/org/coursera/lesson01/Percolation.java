package org.coursera.lesson01;

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/**
 * Created by Alexander Yakovlev on 15/06/2017.
 */
public class Percolation {

    private WeightedQuickUnionUF quickUnionUF;
    private boolean[][] matrix;
    private int size;
    private int opened;

    /**
     * create n-by-n grid, with all sites blocked
     */
    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException();
        }
        size = n;
        matrix = new boolean[size][size];
        quickUnionUF = new WeightedQuickUnionUF(size * size + 2);
    }

    /**
     * open site (row, col) if it is not open already
     */
    public void open(int rowIn, int colIn) {
        if (isOpen(rowIn, colIn)) {
            return;
        }
        int row = rowIn - 1;
        int col = colIn - 1;

        checkBounds(row, col);
        matrix[row][col] = true;
        opened++;
        if (row == 0) {
            quickUnionUF.union(0, getArrIndex(row, col));
        }
        if (row == size - 1) {
            quickUnionUF.union(getLastArrIndex(), getArrIndex(row, col));
        }

        if ((row + 1 < size && isOpen(row + 2, col + 1))) {
            quickUnionUF.union(getArrIndex(row + 1, col), getArrIndex(row, col));
        }
        if (row - 1 >= 0 && isOpen(row, col + 1)) {
            quickUnionUF.union(getArrIndex(row - 1, col), getArrIndex(row, col));
        }
        if (col + 1 < size && isOpen(row + 1, col + 2)) {
            quickUnionUF.union(getArrIndex(row, col + 1), getArrIndex(row, col));
        }
        if (col - 1 >= 0 && isOpen(row + 1, col)) {
            quickUnionUF.union(getArrIndex(row, col - 1), getArrIndex(row, col));
        }
    }

    /**
     * is site (row, col) open?
     */
    public boolean isOpen(int rowIn, int colIn) {
        int row = rowIn - 1;
        int col = colIn - 1;
        checkBounds(row, col);
        return matrix[row][col];
    }

    /**
     * is site (row, col) full?
     */
    public boolean isFull(int rowIn, int colIn) {
        int row = rowIn - 1;
        int col = colIn - 1;
        checkBounds(row, col);
        return quickUnionUF.connected(getArrIndex(row, col), 0);
    }

    /**
     * number of open sites
     */
    public int numberOfOpenSites() {
        return opened;
    }

    /**
     * does the system percolate?
     */
    public boolean percolates() {
        return quickUnionUF.connected(0, getLastArrIndex());
    }

    private int getLastArrIndex() {
        return getArrIndex(getLastMatrixIndex(), getLastMatrixIndex()) + 1;
    }

    private int getLastMatrixIndex() {
        return size - 1;
    }

    private void checkBounds(int row, int col) {
        if (row >= size || row < 0 || col >= size || col < 0) {
            throw new IllegalArgumentException("row=" + row + "; col=" + col);
        }
    }

    private int getArrIndex(int row, int col) {
        return row * size + col + 1;
    }
}
