import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Arrays;
import java.util.Iterator;

public class FastCollinearPoints {

    private final Collection<LineSegment> segments;
    private final List<LineSegmentFull> lsfList = new LinkedList<>();


    // finds all line segments containing 4 or more points
    public FastCollinearPoints(Point[] points) {
        illegalNull(points);
        segments = new ArrayList<>();
        int length = points.length;
        final Point[] p = points;

        for (int i = 0; i < length - 1; i++) {
            illegalNull(p[i]);
            Arrays.sort(p, i + 1, length, p[i].slopeOrder());

            Point basePoint = p[i];

            LineSegmentFull lsf = new LineSegmentFull(basePoint);
            double currSlope;
            double prevSlope = 0.;

            for (int j = i + 1; j < length; j++) {
                if (basePoint.compareTo(p[j]) == 0) {
                    throw new IllegalArgumentException("Duplicated points " + basePoint);
                }
                illegalNull(p[j]);

                currSlope = Math.abs(basePoint.slopeTo(p[j]));

                if (Double.compare(currSlope, prevSlope) == 0) {
                    lsf.addPoint(p[j]);
                } else {
                    findBaseOrAdd(basePoint, lsf);
                    lsf = new LineSegmentFull(basePoint);
                    lsf.addPoint(p[j]);
                }

                prevSlope = currSlope;
            }
            // process last
            findBaseOrAdd(basePoint, lsf);
        }
        while (!lsfList.isEmpty()) { // Empty lsfList if smth left
            segments.add(lsfList.remove(0).getLineSegment());
        }
    }

    /**
     * removes basePoint if it is base for any segment
     * and adds otherwise
     */
    private void findBaseOrAdd(Point basePoint, LineSegmentFull lsf) {
        Iterator<LineSegmentFull> iterator = lsfList.iterator();
        while (iterator.hasNext()) {
            LineSegmentFull next = iterator.next();
            // if basePoint is the base for lineSegment
            if (next.isBase(basePoint, lsf.slope)) {
                segments.add(next.getLineSegment());
                iterator.remove();
                return;

            }
            // if base point lies on the line segment (e.g. has the same slope with max or min)
            if (Double.compare(basePoint.slopeTo(next.max), lsf.slope) == 0 || Double.compare(basePoint.slopeTo(next.min), lsf.slope) == 0) {
                return; // the point is in LineSegment but not the baseЗщште
            }
        }
        if (lsf.counter >= 4) {
            if (lsf.isBase(basePoint, lsf.slope)) {
                segments.add(lsf.getLineSegment());
            } else {
                lsfList.add(lsf);
            }
        }
    }

    // the number of line segments
    public int numberOfSegments() {
        return segments.size();
    }

    // the line segments
    public LineSegment[] segments() {
        return segments.toArray(new LineSegment[numberOfSegments()]);
    }

    private static void illegalNull(Object object) {
        if (object == null) {
            throw new IllegalArgumentException("Point[] is null or containing null element!");
        }
    }

    private static class LineSegmentFull {
        private Point min;
        private Point max;

        private int counter;
        private double slope;

        LineSegmentFull(Point p) {
            addPoint(p);
        }

        public LineSegment getLineSegment() {
            return new LineSegment(min, max);
        }

        public void addPoint(Point p) {
            if (min == null) {
                min = p;
            } else if (max == null) {
                if (p.compareTo(min) < 0) {
                    max = min;
                    min = p;
                } else {
                    max = p;
                }
                slope = min.slopeTo(max);
            }
            if (p.compareTo(min) < 0 && p.compareTo(max) < 0) {
                min = p;
            } else if (p.compareTo(min) > 0 && p.compareTo(max) > 0) {
                max = p;
            }
            counter++;
        }

        public boolean isBase(Point p, double pointSlope) {
            return p.compareTo(min) == 0 && Double.compare(slope, pointSlope) == 0;
        }
    }

    public static void main(String[] args) {
        // read the n points from a file
        // In in = new In(args[0]);
        int n = StdIn.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = StdIn.readInt();
            int y = StdIn.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}