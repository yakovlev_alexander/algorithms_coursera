import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Collection;

public class BruteCollinearPoints {

    private final Collection<LineSegment> segments;

    // finds all line segments containing 4 points
    public BruteCollinearPoints(Point[] points) {
        illegalNull(points);
        segments = new ArrayList<>();
//        if (points.length < 4) {
//            return;
//        }

        byte doubled = 0;
        for (Point p1 : points) {
            illegalNull(p1);
            for (Point p2 : points) {
                illegalNull(p2);
                if (p1.compareTo(p2) == 0 && ++doubled > 1) { // for second doubled
                    throw new IllegalArgumentException("Doubled points!");
                }
                if (p1.compareTo(p2) < 0) {
                    double slope = p1.slopeTo(p2);
                    for (Point p3 : points) {
                        illegalNull(p3);
                        if (p2.compareTo(p3) >= 0 || Double.compare(slope, p1.slopeTo(p3)) != 0 ) {
                            continue;
                        }
                        for (Point p4 : points) {
                            illegalNull(p4);
                            if (p3.compareTo(p4) < 0 && Double.compare(slope, p1.slopeTo(p4)) == 0) {
                                segments.add(new LineSegment(p1, p4));
                            }
                        }
                    }
                }
            }
            doubled = 0;
        }
    }

    // the number of line segments
    public int numberOfSegments() {
        return segments.size();
    }

    // the line segments
    public LineSegment[] segments() {
        return segments.toArray(new LineSegment[numberOfSegments()]);
    }

    private static void illegalNull(Object object) {
        if (object == null) {
            throw new IllegalArgumentException("Point[] is null or containing null element!");
        }
    }

    public static void main(String[] args) {
        // read the n points from a file
//        In in = new In(args[0]);
        int n = StdIn.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = StdIn.readInt();
            int y = StdIn.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
